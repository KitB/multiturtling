#!/usr/bin/env python2.7
import sys
import threading
import pygame
import time

import multiturtling

if __name__ == '__main__':
    fn = sys.argv[1]
    sys.argv = sys.argv[1:]
    s = multiturtling.Screen()

    def ex():
        with open(fn, 'r') as f:
            exec f in {'__name__': '__main__'}

    new_main = threading.Thread(target=ex)
    new_main.daemon = True
    new_main.start()
    while new_main.is_alive():
        pygame.event.pump()
        time.sleep(0.1)
