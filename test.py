#!/usr/bin/env python2.7
import collections
import random
import sys
import threading

import multiturtling
import pygame
import time


def _m():
    t = multiturtling.Turtle()
    try:
        while True:
            t.left(random.randint(-180, 180))
            t.forward(random.randint(0, 100))
    except:
        pass


def _fm1(t):
    try:
        while True:
            t.forward(random.randint(0, 100))
    except:
        pass


def _fm2(t):
    try:
        while True:
            t.left(random.randint(-180, 180))
    except:
        pass


def multi_sequence(n, *args):
    for _ in xrange(int(n)):
        t = threading.Thread(target=_m)
        t.daemon = True
        t.start()
    raw_input()


def full_multi(n, *args):
    for _ in xrange(int(n)):
        bob = multiturtling.Turtle()
        t1 = threading.Thread(target=_fm1, args=(bob,))
        t2 = threading.Thread(target=_fm2, args=(bob,))
        t1.daemon = True
        t2.daemon = True
        t1.start()
        t2.start()
    raw_input()


def interact(*args):
    import IPython

    t = multiturtling.Turtle()  # noqa

    thread = threading.Thread(target=IPython.embed,
                              kwargs={'user_ns': locals()})
    thread.start()
    while thread.is_alive():
        pygame.event.get()
        time.sleep(0.1)

dispatch = collections.defaultdict(lambda: interact)
dispatch['multi_sequence'] = multi_sequence
dispatch['full_multi'] = full_multi
dispatch['interact'] = interact


def main(action, *args):
    dispatch[action](*args)

if __name__ == '__main__':
    main(*sys.argv[1:])
