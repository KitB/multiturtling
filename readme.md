# Multiturtling
A relatively simple Python module reimplementing the basic elements of Python's
`turtle` module in a thread-safe manner.

## Usage
~~~~{.python}
import multiturtling
t = multiturtling.Turtle()
t.forward(100)
~~~~

~~~~{.python}
import random
import threading

import multiturtling


def random_forward(t):
    while True:
        t.forward(random.randint(0, 100))


def random_turn(t):
    while True:
        t.left(random.randint(-180, 180))

for _ in xrange(10):
    bob = multiturtling.Turtle()
    t1 = threading.Thread(target=random_forward, args=(bob,))
    t2 = threading.Thread(target=random_turn, args=(bob,))

    # Ensure the threads exit with the main thread
    t1.daemon = True
    t2.daemon = True

    t1.start()
    t2.start()

# And make sure the main thread doesn't end execution
raw_input()
~~~~

These examples unfortunately won't work on Windows just now as you need to
consume pygame's event queue on Windows otherwise the pygame window will freeze.
On top of this pygame's event queue can only be consumed from the main thread.
As a result you will need to replace the `raw_input()` line above with code like
the following:

~~~~{.python}
import pygame
import time

th = threading.Thread(target=raw_input)
th.start()
while th.is_alive():
    pygame.event.pump()
    time.sleep(0.1)
~~~~

## Dependencies
Needs pygame for drawing. If IPython is installed then `test.py` can make use of
it.

## `test.py` examples
* `./test.py interact` (only if IPython is installed). Will drop into an IPython
  shell with a turtle available under the name `t`.
* `./test.py multi_sequence 10`
* `./test.py full_multi 10`
