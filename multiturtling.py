from __future__ import division
import math
import time

import threading

import pygame
import pygame.display
import pygame.time
from pygame import draw
from pygame import transform


def roundint(f):
    return int(round(f))


def bound(val, lower, upper=None):
    upper = upper or -lower

    dist = abs(upper - lower)
    return ((val + (dist / 2)) % dist) - (dist / 2)


class Highlander(type):
    """ THERE CAN ONLY BE ONE
        (actually a singleton)"""
    def __init__(cls, *args, **kwargs):
        super(Highlander, cls).__init__(*args, **kwargs)
        cls.instance = None
        cls.instance_lock = threading.Lock()

    def __call__(cls, *args, **kwargs):
        with cls.instance_lock:
            if cls.instance is None:
                cls.instance = super(Highlander, cls).__call__(*args, **kwargs)

            return cls.instance


class Screen(threading.Thread):
    __metaclass__ = Highlander
    daemon = True

    def __init__(self, dimensions=(800, 800), *args, **kwargs):
        super(Screen, self).__init__(*args, **kwargs)
        if not pygame.display.get_init():
            pygame.init()
        self.screen = pygame.display.set_mode(dimensions)
        self.dimensions = dimensions
        self._drawables = set()
        self._drawable_lock = threading.Lock()

        self.bgcolor = (255, 255, 255)

        self.clock = pygame.time.Clock()

        self.start()

    def add_drawable(self, drawable):
        with self._drawable_lock:
            self._drawables.add(drawable)

    def run(self):
        while True:
            self.screen.fill(self.bgcolor)
            with self._drawable_lock:
                for d in self._drawables:
                    d.draw(self.screen)
            pygame.display.flip()
            self.clock.tick(60)


class Line(object):
    def __init__(self, start_pos, end_pos, color=(0, 0, 0)):
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.color = color

    def draw(self, screen):
        draw.line(screen, self.color, self.start_pos, self.end_pos)


class Paper(object):
    __metaclass__ = Highlander

    def __init__(self):
        screen = Screen()
        self._surface = pygame.surface.Surface(screen.dimensions)
        self._surface.fill(screen.bgcolor)
        self._surface.set_colorkey(screen.bgcolor)

    def add_drawable(self, drawable):
        drawable.draw(self._surface)

    def draw(self, screen):
        screen.blit(self._surface, (0, 0))


class Turtle(object):
    TURN_AMOUNT = 0.001
    TURN_DELAY = 2e-6

    def __init__(self):
        self.screen = Screen()
        self.paper = Paper()

        self.x, self.y = 400, 400

        self.orientation = math.pi / 2

        self.pen_color = (0, 0, 0)
        self.pen_down = True

        self.screen.add_drawable(self)
        self.screen.add_drawable(self.paper)

    @property
    def position(self):
        return (roundint(self.x), roundint(self.y))

    @property
    def pen_color(self):
        return self._pen_color

    @pen_color.setter
    def pen_color(self, color):
        self._pen_color = color
        try:
            del self._drawable
        except AttributeError:
            pass

    def up(self):
        self.pen_down = False

    def down(self):
        self.pen_down = True

    def _forward(self, amount=1):
        start_pos = self.position
        self.x += amount * math.sin(self.orientation)
        self.y += amount * math.cos(self.orientation)
        end_pos = self.position
        if self.pen_down:
            self.paper.add_drawable(Line(start_pos, end_pos, self.pen_color))

    def _left(self, amount=None):
        amount = amount or self.TURN_AMOUNT
        self.orientation += amount

    def forward(self, amount):
        for _ in xrange(amount):
            self._forward()
            time.sleep(0.001)

    def backward(self, amount):
        for _ in xrange(amount):
            self._forward(-1)
            time.sleep(0.001)

    def left(self, amount):
        if amount < 0:
            self.right(-amount)
            return
        target = self.orientation + math.radians(amount)
        while self.orientation < target:
            self._left(self.TURN_AMOUNT)
            time.sleep(self.TURN_DELAY)

    def right(self, amount):
        if amount < 0:
            self.left(-amount)
            return
        target = self.orientation - math.radians(amount)
        while self.orientation > target:
            self._left(-self.TURN_AMOUNT)
            time.sleep(self.TURN_DELAY)

    def draw(self, screen):
        rotated = transform.rotate(self.drawable,
                                   math.degrees(self.orientation + math.pi))
        w, h = rotated.get_size()
        scaled = transform.smoothscale(rotated, (w // 2, h // 2))
        w, h = scaled.get_size()
        screen.blit(scaled, (self.x - (w // 2), self.y - (h // 2)))

    def _make_drawable(self):
        self._drawable = pygame.surface.Surface((40, 40))
        self._drawable.fill(self.screen.bgcolor)
        self._drawable.set_colorkey(self.screen.bgcolor)
        draw.polygon(self._drawable, self.pen_color,
                     [(20, 0), (40, 40), (20, 25), (0, 40)])

    @property
    def drawable(self):
        try:
            return self._drawable
        except AttributeError:
            self._make_drawable()
            return self._drawable
